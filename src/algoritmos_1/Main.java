package algoritmos_1;

import java.util.Scanner;

/**
 *
 * @author Nocsabe
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("***-----------------------------***\n"
                + "Multiplicacion de matrices binarias\n"
                + "**------------------------------***");
        System.out.println("Digite a continuacion las dimensiones de la matriz 1. (m x n). ");
        int fila_m1 = sc.nextInt();
        int col_m1 = sc.nextInt();
        System.out.println("Digite ahora las dimensiones de la matriz 2. ");
        int fila_m2 = sc.nextInt();
        int col_m2 = sc.nextInt();
        if (col_m1 == fila_m2) {
            SistemaMatrizBinaria sistema = new SistemaMatrizBinaria(fila_m1, col_m1, fila_m2, col_m2);
            System.out.println("desea visualizar las matrices de entrada?. y/n");
            String verMatrices = sc.next();
            if (verMatrices.equalsIgnoreCase("y")) {
                System.out.println("Matriz 1. ");
                System.out.println(sistema.showMatriz(sistema.getM1()));
                System.out.println("Matriz 2.");
                System.out.println(sistema.showMatriz(sistema.getM2()));
            }
            System.out.println("Matriz resultante. ");
            System.out.println(sistema.showMatriz(sistema.getMultiplicacion()));

        } else {
            System.out.println("El numero de columnas de la matriz uno debe ser igual a numero de filas de la matriz dos.");
        }
    }

}
