package algoritmos_1;

/**
 *
 * @author Nocsabe
 */
public class SistemaMatrizBinaria {

    private boolean m1[][];
    private boolean m2[][];

    public SistemaMatrizBinaria() {
    }

    public SistemaMatrizBinaria(int fila_m1, int col_m1, int fila_m2, int col_m2) {
        m1 = new boolean[fila_m1][col_m1];
        m2 = new boolean[fila_m2][col_m2];
        this.llenarMatriz();
    }
    
    /**
     * llenar matriz dado el numero de palabras y la cantidad de bits.
     * O (2m x n + 2p x q)
     */
    private void llenarMatriz() {
        boolean listBinary[];
        
        //O(2m x n)
        for (int i = 0; i < m1.length; i++) {
            listBinary = IntegerToListBinary(i, m1[i].length); // O(m)
            for (int j = 0; j < m1[i].length; j++) {
                m1[i][j] = listBinary[j];
            }
        }
        
        // ((2p x q) 
        for (int i = 0; i < m2.length; i++) {
            listBinary = IntegerToListBinary(i, m2[i].length); // O(p)
            for (int j = 0; j < m2[i].length; j++) {
                m2[i][j] = listBinary[j];
            }
        }
           
    }

    /**
     * Retorna un arreglo de booleanos con la cantidad de bits que se indican.
     *
     * @param number
     * @param bits
     * @return
     */
    public boolean[] IntegerToListBinary(int number, int bits) {
        boolean listBinary[] = new boolean[bits];
        String s = Integer.toBinaryString(number);
        for (int i = s.length() - 1, j = bits - 1; i >= 0; i--, j--) {
            listBinary[j] = s.charAt(i) == '1';
        }
        return listBinary;
    }

    /**
     * multiplica las dos matrices m1 y m2.
     * O (n^3)
     * @return m3
     */
    public boolean[][] getMultiplicacion() {
        boolean m3[][] = new boolean[m1.length][m2[0].length];
        for (int i = 0; i < m3.length; i++) {
            for (int j = 0; j < m3[i].length; j++) {
                for (int k = 0; k < m2.length; k++) {
                    if (!m3[i][j]) {
                        m3[i][j] = sumarBinarios(m3[i][j], multiplicarBinarios(m1[i][k], m2[k][j]));
                    } else {
                        break;
                    }

                }
            }
        }
        
        return m3;
    }

    /**
     * Suma dos booleanos y retorna el resultado
     *
     * @param a
     * @param b
     * @return a or b
     */
    public boolean sumarBinarios(boolean a, boolean b) {
        return a || b;
    }

    /**
     * Multiplica dos booleanos
     *
     * @param a
     * @param b
     * @return a and b
     */
    public boolean multiplicarBinarios(boolean a, boolean b) {
        return a && b;
    }

    /**
     * Retorna un string que representa la matriz.
     *
     * @param matriz
     * @return
     */
    public String showMatriz(boolean matriz[][]) {
        String s = "";
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                s += matriz[i][j] ? 1 : 0;
            }
            s += "\n";
        }

        return s;
    }

    public boolean[][] getM1() {
        return m1;
    }

    public void setM1(boolean[][] m1) {
        this.m1 = m1;
    }

    public boolean[][] getM2() {
        return m2;
    }

    public void setM2(boolean[][] m2) {
        this.m2 = m2;
    }

}
